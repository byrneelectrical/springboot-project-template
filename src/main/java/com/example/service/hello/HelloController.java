package com.example.service.hello;

import com.example.service.hello.api.World;
import com.example.service.hello.entity.WorldEntity;
import com.example.service.hello.entity.WorldRepository;
import java.net.URI;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.persistence.EntityNotFoundException;
import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/hello")
public class HelloController {

  private WorldRepository worldRepostiory;

  @Autowired
  public HelloController(WorldRepository worldRepo) {
    this.worldRepostiory = worldRepo;
  }

  @RequestMapping(path="/world", method= RequestMethod.POST)
  public ResponseEntity<World> createWorld(@RequestBody World world) {
    try {
      WorldEntity entity = WorldMapper.fromApi(world);

      Long id = worldRepostiory.save(entity).getId();
      URI uri = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(id).toUri();

      return ResponseEntity.created(uri).body(null);
    } catch (Exception e) {
      return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(null);
    }
  }

  @RequestMapping(path="/world", method= RequestMethod.GET)
  public ResponseEntity<List<World>> getAllWorlds() {
    try {

      List<World> worlds = StreamSupport.stream(worldRepostiory.findAll().spliterator(), false)
          .map( WorldMapper::fromEntity ).collect(Collectors.toList());

      return ResponseEntity.ok(worlds);
    } catch (Exception e) {
      return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(null);
    }
  }

  @RequestMapping(path="/world/{id}", method= RequestMethod.PUT)
  public ResponseEntity<World> updateWorld(@PathVariable Long id, @RequestBody World updateWorld) {
      WorldEntity entity = worldRepostiory.getOne(id);
      entity.updateFrom(updateWorld);

      worldRepostiory.save(entity);

      return ResponseEntity.status(HttpStatus.ACCEPTED).body(null);
  }


  @RequestMapping(path="/world/{id}", method= RequestMethod.GET)
  public ResponseEntity<World> getWorld(@PathVariable Long id) {
      WorldEntity world = worldRepostiory.getOne(id);
      return ResponseEntity.ok(WorldMapper.fromEntity(world));
  }

  @RequestMapping(path="/world/{id}", method= RequestMethod.DELETE)
  public ResponseEntity<World> deleteWorld(@PathVariable Long id) {
      worldRepostiory.delete(id);
      return ResponseEntity.accepted().body(null);
  }

    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    @ExceptionHandler({NumberFormatException.class})
    @ResponseBody String handleBadRequest(HttpServletRequest request, Exception ex) {
        return String.format("%s: Invalid world id", request.getRequestURL());
    }

  @ResponseStatus(value = HttpStatus.NOT_FOUND)
  @ExceptionHandler({EntityNotFoundException.class, EmptyResultDataAccessException.class})
  @ResponseBody String handleNotFound(HttpServletRequest request, Exception ex) {
      return String.format("%s: World not found", request.getRequestURL());
  }



}
