# Welcome to the Service Project Template!

## Purpose
This project is intended to be a template for new web service projects.  It has all the essential elements to be able
to quickly bring up a project and have it working.

## Getting Started

#### Necessary software
In order to write code for this application you will, at a minimum, need to install the following:
- JDK 1.8 - http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html
- Maven 3 - https://maven.apache.org/download.cgi

#### To use a local database, MySQL and Docker are an easy-on option
- Docker - https://docs.docker.com/docker-for-windows/
- Run the following command: ```
docker run --name notifier-mysql -p 3306:3306 -e MYSQL_ROOT_PASSWORD=[my-secret-pw] -d mysql:latest
```
Now you should be able to access MySQL from any of the available clients at localhost:3306 with the user 'root' and the password that you set in the command line argument.  Since the ORM generates the sql, there will be no dialect or compatibility issues.  Just set the correct drivers and URLs in your application.yml file

#### Running tests and compiling the project
For this project we are using Maven as the build and dependency management tool.  Everything that you need is baked into the build process including downloading dependencies, running tests, measuring code coverage, and packaging the appliction for deployment. However, sometimes you don't want to run everything at once, so some of the following commands can help.

Clean out anything from previous builds. Maven normally only builds what is necessary so unchanged classes are not replaced.  Very often, during the software development process, using clean is part of making sure errors aren't from previous builds:
```
mvn clean
```

You can chain goals together, this example will clean first, then run all the tests:
```
mvn clean test
```

When you want to build a deployable package, you will want to use the package goal.  Because package is further down the lifecycle than test, your tests will be run as well:
```
mvn clean package
```

This application is built with a popular framework called Spring Boot.  It has its own extension that will automatically start the application in development mode.
```
mvn clean spring-boot:run
```

If you would like to step through the application in a debugger, port 8081 is where that is available.  You can easily connect to it from an IDE like Eclipse or Intellij and step through the running application.

## Deployment

Contained in this template are example gitlab-ci and Dockerfile templates.  If your web service is simple these should have all you ned to allow
gitlab to build, test, provide test coverage and create a docker image from your service.  Once you have that you can log into the
dev server and easily deploy the files.  To set up your new service, add a <service name>.dockerproperties file to the config section of the [vm-management project](https://gitlab.com/byrneelectrical/vm-management).

After adding your docker properties configuration and pushing to gitlab you can pull it down in /opt/byrne/build and then run the deploy and promote scripts.

To deploy the project, run:
```
deploy-container.sh project-name #(from project-name.dockerproperties)
```

deploy-container.sh can also accept -t for a tag(if you want to deploy something other than latest) and -o to specify different options than those specified in the property file.

To promote a project, run:
```
sudo -u dev promote-container.sh project-name
```
This will promote the container to production.  By specifying the user, you will use the deployment keys that are already in place so you don't need to set them up on your own account.


## Build and Coverage batches
The following are examples of code coverage and build status.  You can copy and expand to other branches as desired.

### Master

[![build status](https://gitlab.com/byrneelectrical/springboot-project-template/badges/master/build.svg)](https://gitlab.com/byrneelectrical/springboot-project-template/commits/master)

[![coverage report](https://gitlab.com/byrneelectrical/springboot-project-template/badges/master/coverage.svg)](https://gitlab.com/byrneelectrical/springboot-project-template/commits/master)


### Develop
[![build status](https://gitlab.com/byrneelectrical/springboot-project-template/badges/develop/build.svg)](https://gitlab.com/byrneelectrical/springboot-project-template/commits/develop)

[![coverage report](https://gitlab.com/byrneelectrical/springboot-project-template/badges/develop/coverage.svg)](https://gitlab.com/byrneelectrical/springboot-project-template/commits/develop)